## Docker Setup  
In command/bash execute `docker-compose up` and then access the single page in http://localhost:3000  

## Local Setup  
### Local backend api Setup  
Configure enviroment variables for the database DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD  
* npm install 
* npm start 

### Local frontend setup  
* npm install  
* npm start  
