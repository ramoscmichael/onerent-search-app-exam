const { ApolloServer } = require('apollo-server-express');
const { TYPES_DEFINITIONS } = require('./schema/types.js');
const { RESOLVERS } = require('./schema/resolvers.js');

const graph = new ApolloServer({
    typeDefs: TYPES_DEFINITIONS,
    resolvers: RESOLVERS,
    playground: {
        endpoint: '/graphql',
        settings: {
            'editor.theme': 'light'
        }
    }
});

module.exports = {
    graph
}
