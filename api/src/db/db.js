const uuid = require('uuid');
const data = require('../data/data.json');

const { Sequelize } = require('sequelize');
const sequelize = new Sequelize({
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    host: process.env.DATABASE_HOST,
    dialect: 'postgres'
});

const User = sequelize.define('user', {
    id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
    },
    firstName: { type: Sequelize.STRING },
    lastName: { type: Sequelize.STRING }
});

const Property = sequelize.define('property', {
    id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
    },
    userId: {
        type: Sequelize.UUID,
        references: {
            model: 'users',
            key: 'id'
        }
    },
    street : { type: Sequelize.STRING },
    city:{ type: Sequelize.STRING },
    state: { type: Sequelize.STRING },
    zip: { type: Sequelize.STRING },
    rent: { type: Sequelize.DOUBLE }
});

User.hasMany(Property);
Property.belongsTo(User);

//dump data
(async (user) => {
    await User.sync({force: true});
    await Property.sync({force: true});

    await User.bulkCreate(user.map(o => ({ id: o.id, firstName: o.firstName, lastName: o.lastName })));
    await Property.bulkCreate(user.reduce((groups, u) => groups.concat(u.properties), []));

    console.log('done insert test data');
})(data.users.map(u => {
    u.id = uuid.v4();
    u.properties = u.properties.map(p => {
        p.id = uuid.v4();
        p.userId = u.id;
        return p;
    });
    return u;
}));

module.exports = {
    User,
    Property,
}