const { Op } = require("sequelize");
const { User, Property } = require('../db/db.js');

function createSearchCriteria(text) {
    return (...fields) => {
        const filters = (text || '').split(/\s/g).map(o => o.trim()).filter(o => !!o);
        return fields.reduce((criteria, field) => criteria.concat(filters.map(fragment => ({
            [field]: {  [Op.iLike]: `%${fragment}%` }
        }))), []);
    };
}

async function search(parent, args, context, info) {
    const users = await User.findAll({
        where: {
            [Op.or]: createSearchCriteria(args.text)('firstName', 'lastName')
        },
        include: [{ model: Property }]
    });

    const properties = await Property.findAll({
        where: {
            [Op.or]: createSearchCriteria(args.text)('city', 'street', 'zip', 'state')
        },
        include: [{ model: User }]
    });

    return {
        users,
        properties
    };
}

const RESOLVERS = {
    Query: {
        search
    }
};

module.exports = {
    RESOLVERS
};