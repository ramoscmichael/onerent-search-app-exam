const { gql } = require('apollo-server-express');

const TYPES_DEFINITIONS = gql`
    type Query { search(text: String): SearchResult }
    type User { id: String, firstName: String, lastName: String, properties: [Property] }
    type Property { id: String, street: String, city: String, state: String, zip: String, rent: Float, user: User }
    type SearchResult { users: [User], properties: [Property] }
`;

module.exports = {
    TYPES_DEFINITIONS
};