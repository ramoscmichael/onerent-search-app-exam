const express = require('express');
const { graph } = require('./graph.js');

// Initialize the app
const app = express();

graph.applyMiddleware({ app });

// Start the server
app.listen(4000, () => {
    console.log(`Go to http://localhost:${process.env.PORT || 4000}/graphiql to run queries!`);
});