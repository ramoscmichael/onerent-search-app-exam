import { ApolloClient, gql, InMemoryCache } from '@apollo/client';

// todo transfer to environment variables
const GRAPH_URI='http://localhost:4000/graphql'

const client = new ApolloClient({
  uri: GRAPH_URI,
  cache: new InMemoryCache(),
});

function SearchService() {
    this.search = async (text) => {
        const result = {
            users: [],
            properties: [],
            error: undefined,
        };

        await client.query({
            query: gql`
            query ($searchText: String){
                search(text: $searchText) {
                    users {
                        id,
                        firstName,
                        lastName,
                        properties {
                            id,
                            zip,
                            city,
                            street,
                            state,
                            rent
                        }
                    },
                    
                    properties {
                        id,
                        zip,
                        city,
                        street,
                        state,
                        rent
                    }
                }
            }`,
            variables: {
                searchText: text
            }
        }).then((resp) => {
            result.users = resp.data.search.users;
            result.properties = resp.data.search.properties;
        }).catch(error => {
            result.error = error;
        });

        return result;
    }

}

export default SearchService;