import React from 'react';
import Search from './components/search/Search.js';
import Users from './components/user/Users.js';
import Properties from './components/property/Properties.js';
import SearchService from './services/SearchService.js';
import './App.css';

const INITIAL_MESSAGE = 'Start searching properties now.';
const NO_RECORDS_MESSAGE = 'No matching record(s) found.';

class App extends React.Component {
  service = new SearchService();
  constructor() {
    super();
    this.state = {
      users: [],
      properties: [],
      message: 'Start searching properties now.'
    };
  }

  async onSearch(e) {
    if(e.trim()) {
      const results = await this.service.search(e);
      this.setState({
        users: results.users,
        properties: results.properties,
        message: (!results.users.length && !results.properties.length) ? NO_RECORDS_MESSAGE : undefined
      });
    } else {
      this.setState({
        users: [],
        properties: [],
        message: INITIAL_MESSAGE
      });
    }
  }

  render() {
    return (
          <div className="App">
            <div>
              <h1>Onerent Property Finder</h1>
            </div>
            <Search key="search" onSearch={this.onSearch.bind(this)}/>
            {
              this.state.message ? 
              <div className="message">{this.state.message}</div> :
              <div>
                <Users key="users" users={this.state.users}/>
                <Properties key="properties" properties={this.state.properties}/>
              </div>
            }
            
          </div>
    );
  }
  
}

export default App;
