import React from 'react';
import './Properties.css';

function PropertyField(prop) {
    return (
        <div className="propertyField">
            <div className="propertyFieldValue">{prop.value}</div>
            <div className="propertyFieldLabel">{prop.label}</div>
        </div>
    )
}

function Property(props) {
    return (
        <div>
            <div className="propertyCard">
                <PropertyField label="street" value={props.property.street}/>
                <PropertyField label="city" value={props.property.city}/>
                <PropertyField label="state" value={props.property.state}/>
                <PropertyField label="zip" value={props.property.zip}/>
                <PropertyField label="rent" value={props.property.rent}/>
            </div>
        </div>
    );
}

export function PropertyList(props) {
    return (
        <div className="row">
             {props.properties.map((property, i) => (
                 <div key={property.id + props.id} className="column">
                    <Property  property={property}/>
                 </div>
             ))}
        </div>
    );
}


export default function Properties(props) {
    const properties = props.properties || [];
    const propertiesDisplay = properties.length > 0 ?
                <div className="resultMain">
                    <span className="resultLabel">Properties</span>
                    <PropertyList properties={properties} id="properties"/>
                </div> : <div></div>
    return propertiesDisplay;
}