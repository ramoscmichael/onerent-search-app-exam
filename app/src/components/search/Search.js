import React from 'react';
import './Search.css';

const debounce = (func, delay) => {
  let inDebounce
  return function() {
    const context = this
    const args = arguments
    clearTimeout(inDebounce)
    inDebounce = setTimeout(() => func.apply(context, args), delay)
  }
}

class Search extends React.Component {

  constructor() {
    super();

    this.state = {
      searchText: ''
    };
  }

  onSearch (e) {
    this.setState({ searchText: e.target.value });
    if(!this.debounceSearch) {
      this.debounceSearch = debounce(async () => {
        if (this.props.onSearch) {
          this.props.onSearch(this.state.searchText);
        }
      }, 400);
    }

    this.debounceSearch();
  }

  render() {
    return (
      <div className="searchMain">
          <span>
            <img alt="search" src="/search.png"></img>
          </span>
          <input placeholder="Search" value={this.state.searchText} onChange={this.onSearch.bind(this)}></input>
      </div>
    );
  }
}

export default Search;
