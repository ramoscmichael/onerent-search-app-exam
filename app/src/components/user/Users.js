import React from 'react';
import './Users.css';
import { PropertyList } from '../property/Properties.js'

function User(props) {
    const user = props.user || {};
    const properties = user.properties || [];

    return (
        <div className="userInfo">
            <div>{user.firstName} {user.lastName}</div>
            <PropertyList properties={properties} id="user-properties"/>
        </div>
    )
}

export default function Users(props) {
    const users = props.users || [];
    const usersDisplay = users.length > 0 ? (
        <div className="resultMain">
            <span className="resultLabel">Owners</span>
            <div className="resultAreaUser">
                {users.map(user => (<User key={user.id + '-user'} user={user}/>))}
            </div>
        </div>
    ) : <div></div>;
    return usersDisplay
}